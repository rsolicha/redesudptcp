import socket
import time
import os
import sys

ROOT = '/filesTCP'

"""Utilizada para enlistar todos los archivos que hay en el servidor"""
def listaArchivos():
    print("Solicitud de obtener lista de archivos")
    F = os.listdir(ROOT)
    listaArchivos = []
    for file in F:
        listaArchivos.append(file)
    listaStrings = str(listaArchivos)
    listaCodificada = listaStrings.encode('utf-8')
    data.send(listaCodificada)
    print("Lista enviada al cliente")


"""Funcion utilizada para descargar un archivo para el cliente"""
def descargaArchivo(archivo):
    print("Solicitud de descargar un archivo")

    if os.path.isfile(ROOT+"/"+archivo):
        print("El archivo existe")

        # DescomposiciOn del archivo
        numPaquete = 0
        tamannoPaquete = os.stat(ROOT+"/"+archivo)
        tamannoPaqueteObtenido = tamannoPaquete.st_size  
    
        totalPaquetes = int(tamannoPaqueteObtenido / 1024) 
        totalPaquetes = totalPaquetes + 1

        encapsulamientoPaquete = str(totalPaquetes)
        encapsulamientoPaqueteCodificado = encapsulamientoPaquete.encode('utf8')
        data.send(encapsulamientoPaqueteCodificado)

        cicloPaquete = int(totalPaquetes)

        obtenerArchivo = open(ROOT+"/"+archivo, "rb")
        while cicloPaquete != 0:
            paqueteArchivo = obtenerArchivo.read(1024)
            data.send(paqueteArchivo)
            numPaquete += 1
            cicloPaquete -= 1
        obtenerArchivo.close()
        print("Archivo enviado al cliente")
        
    else:
        encapsulamientoPaquete = "-1"
        encapsulamientoPaqueteCodificado = encapsulamientoPaquete.encode('utf8')
        data.send(encapsulamientoPaqueteCodificado)
        print("Imposible enviar archivo al cliente")


"""Usado para subir un archivo"""
def subirArchivo():
    print("Solicitud de subir un archivo")
    detector = open(ROOT+"/"+listaArgumento[1], "wb")
    ClientBData = data.recv(1024)
    while ClientBData:
        detector.write(ClientBData)
        ClientBData = data.recv(1024)
    print("Nuevo archivo subido")


"""Cuando un parametro no existe se corre dicha funcion"""
def incongruenciaParametros():
    mensaje = "La correcta forma de ejecutar es: {IP PUERTO CLAVE(-d, -auxSocket, -l) ARCHIVO}"
    mensajeCodificado = mensaje.encode('utf-8')
    data.send(mensajeCodificado, direccionCliente)
    print("Incongruencia detectada y enviada")


"""Definicion de parametros para el servidor"""
host = ""
port = 5001
try:
    auxSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Socket del servidor inicializado")
    auxSocket.bind((host, port))
    auxSocket.listen(1)
    print("Conexion satisfactoria. Esperando respuesta de cliente")
except socket.error:
    print("Fallo al crear el Socket")
    sys.exit()


"""Funcion que espera envio de datos por parte del cliente"""
while True:
    try:
        data, direccionCliente = auxSocket.accept()
    except ConnectionResetError:
        print("Numero de puerto inconsistente")
        sys.exit()
    texto = data.recv(1024).decode("utf-8")
    listaArgumento = texto.split()
    print("")
    print("<-- Nuevo argumento detectado -->")
    if listaArgumento[0] == "-d":
        descargaArchivo(listaArgumento[1])
    elif listaArgumento[0] == "-u":
        subirArchivo()
    elif listaArgumento[0] == "-l":
        listaArchivos()
    else:
        incongruenciaParametros()