import socket
import time
import os
import sys
        
###########################################################################
######################      FUNCIONES       ###############################
###########################################################################
"""Verifica si se reciben entre cuatro o tres argumentos que son:
        {IP, PUERTO, CLAVE(-d, -auxSocket, -l), ARCHIVO}"""
def verificarArgumentos():
    if len(sys.argv) == 4:
        if(sys.argv[3]!='-l'):
            print("Solo la operación obtener lista puede recibir 3 argumentos")
            sys.exit()
        else:
            print("Cantidad de parámetros aceptados")
    elif len(sys.argv) != 5:
        print("Cantidad de parámetros insuficientes: {IP PUERTO CLAVE(-d, -auxSocket, -l) ARCHIVO}")
        sys.exit()
    else:
        print("Cantidad de parámetros aceptados")


"""Verifica el puerto colocado"""
def verificarPuerto():
    if int(sys.argv[2]) != 5002:
        print("El puerto debe ser 5002 (UDP)")
        sys.exit()
    else:
        print("Puerto Aceptado")
    

"""Descarga un archivo desde el servidor"""
def iniciarDescarga():
    print("Detección comando descarga")
    try:
        infoCliente, dirCliente = auxSocket.recvfrom(51200)
    except ConnectionResetError:
        print("Los puertos no coinciden")
        sys.exit()
    except:
        print("Tiempo de espera agotado")
        sys.exit()
    texto = infoCliente.decode('utf8')
    print("¿Comando encontrado?: "+texto)

    try:
        infoCliente2, dirCliente2 = auxSocket.recvfrom(51200)
    except ConnectionResetError:
        print("Los puertos no coinciden")
        sys.exit()
    except:
        print("Tiempo de espera agotado")
        sys.exit()
    texto2 = infoCliente2.decode('utf8')
    if texto2 == "El archivo existe!":
        print("¿Existe el archivo?: "+texto2)
        if len(texto2) < 30:
            detector = open("recibido-" + sys.argv[4], "wb")
            try:
                contador, contadorDir = auxSocket.recvfrom(4096)
            except ConnectionResetError:
                print("Los puertos no coinciden")
                sys.exit()
            except:
                print("Tiempo de espera agotado")
                sys.exit()

            almacen = contador.decode('utf8')
            almacenCiclo = int(almacen)
            while almacenCiclo != 0:
                ClientBData, clientbAddr = auxSocket.recvfrom(4096)
                informacion = detector.write(ClientBData)
                almacenCiclo = almacenCiclo - 1

            detector.close()
            print("Archivo descargado. Comprueba su descarga!")
    else:
        print("Archivo no existe")


"""Sube un archivo al servidor"""
def iniciarSubida():
    print("Detección comando subida")
    try:
        contador, contadorDir = auxSocket.recvfrom(4096)
    except ConnectionResetError:
        print("Los puertos no coinciden")
        sys.exit()
    except:
        print("Tiempo de espera agotado")
        sys.exit()

    respuestaServidor = contador.decode('utf8')
    if respuestaServidor == "Comando subida de archivo iniciada":
        if os.path.isfile(sys.argv[4]):
            tamanno = os.stat(sys.argv[4])
            tamannoPaquete = tamanno.st_size  # numero de paquetes  

            numeroPaquete = int(tamannoPaquete / 4096)
            numeroPaquete = numeroPaquete + 1

            almacen = str(numeroPaquete)
            almacenCiclo = almacen.encode('utf8')
            auxSocket.sendto(almacenCiclo, contadorDir)

            almacenIndiceCiclo = int(numeroPaquete)
            archivoCorriendo = open(sys.argv[4], "rb")
            while almacenIndiceCiclo != 0:
                corriendo = archivoCorriendo.read(4096)
                auxSocket.sendto(corriendo, contadorDir)               
                almacenIndiceCiclo -= 1      
            archivoCorriendo.close()
            print("Archivo subido")
        else:
            print("Archivo no existe")
    else:
        print("Imposible subir archivo")


"""Devuelve los archivos que están en el servidor"""
def iniciarEnlistado():
    print("Detección comando listado")
    try:
        contador, contadorDir = auxSocket.recvfrom(4096)
    except ConnectionResetError:
        print("Los puertos no coinciden")
        sys.exit()
    except:
        print("Tiempo de espera agotado")
        sys.exit()

    respuestaServidor = contador.decode('utf8')
    if respuestaServidor == "Comando lista de archivos iniciada":
        listaInfoCliente, ClientDireccionL = auxSocket.recvfrom(4096)
        infoLista = listaInfoCliente.decode('utf8')
        print(infoLista)
        print("Lista archivos obtenida")
    else:
        print("Lista de archivos no obtenida")

###########################################################################
######################      EJECUCIONES     ###############################
###########################################################################
# Verifica los argumentos
verificarArgumentos()

# Verifica la ip dada
try:
    socket.gethostbyname(sys.argv[1])
except socket.error:
    print("IP inválida")
    sys.exit()
host = sys.argv[1]

# Verifica el puerto
try:
    port = int(sys.argv[2])
except ValueError:
    print("Puerto Inválido")
    sys.exit()
except IndexError:
    print("Puerto Inválido")
    sys.exit()
verificarPuerto()

#Inicializar socket 
try:
    auxSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("Socket Cliente inicializado")
    auxSocket.setblocking(0)
    auxSocket.settimeout(15)
except socket.error:
    print("Fallo al crear socket cliente")
    sys.exit()

# Verificar comando
try:
    comando = sys.argv[3]
    if comando != "-d" and comando != "-u" and comando != "-l":
        print("Comando Inválido")
        sys.exit()
    else:
         print("Comando Aceptado: "+ comando )
except ValueError:
    print("Comando Inválido")
    sys.exit()
except IndexError:
    print("Comando Inválido")
    sys.exit()

# Conexión con el servidor
if comando == "-d" or comando == "-u": ##se manda dos parámetros
    command = comando + " "+ sys.argv[4]
    print(command)
else:
    command = comando
CommClient = command.encode('utf-8')
try:
    auxSocket.sendto(CommClient, (host, port))
except ConnectionResetError:
    print( "Incongruencia con los puertos")
    sys.exit()

# División de tareas a partir de comando
if comando == "-d":
    iniciarDescarga()
elif comando =="-u":
    if os.path.isfile(sys.argv[4]):
        iniciarSubida()
    else:
        print("Archivo no existe")
elif comando =="-l":
    iniciarEnlistado()
else:
    try:
        contador, contadorDir = auxSocket.recvfrom(4096)
    except ConnectionResetError:
        print("Los puertos no coinciden")
        sys.exit()
    except:
        print("Tiempo de espera agotado")
        sys.exit()
    text = contador.decode('utf8')
    print(text)

print("¡Aplicación finalizada correctamente!") 
quit()