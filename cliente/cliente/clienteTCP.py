import socket
import time
import os
import sys

###########################################################################
######################      FUNCIONES       ###############################
###########################################################################
"""Verifica si se reciben entre cuatro o tres argumentos que son:
        {IP, PUERTO, CLAVE(-d, -auxSocket, -l), ARCHIVO}"""
def verificarArgumentos():
    if len(sys.argv) == 4:
        if(sys.argv[3]!='-l'):
            print("Solo la operación obtener lista puede recibir 3 argumentos")
            sys.exit()
        else:
            print("Cantidad de parámetros aceptados")
    elif len(sys.argv) != 5:
        print("Cantidad de parámetros insuficientes: {IP PUERTO CLAVE(-d, -auxSocket, -l) ARCHIVO}")
        sys.exit()
    else:
        print("Cantidad de parámetros aceptados")


"""Verifica el puerto colocado"""
def verificarPuerto():
    if int(sys.argv[2]) != 5001:
        print("El puerto debe ser 5001 (TCP)")
        sys.exit()
    else:
        print("Puerto Aceptado")
    

"""Descarga un archivo desde el servidor"""
def iniciarDescarga():
    print("Detección comando descarga")
    contador = auxSocket.recv(1024)
    respuestaServidor = contador.decode('utf8')
    if respuestaServidor != "-1":
        detector = open("recibido-" + sys.argv[4], "wb")
        print(contador)
        almacen = contador.decode('utf8')
        almacenCiclo = int(almacen)
        while almacenCiclo != 0:
            ClientBData = auxSocket.recv(1024)
            informacion = detector.write(ClientBData)
            almacenCiclo = almacenCiclo - 1
        print("Archivo descargado. Comprueba su descarga!")
    else:
        print("Archivo no encontrado")


"""Sube un archivo al servidor"""
def iniciarSubida():
    print("Detección comando subida")
    if os.path.isfile(sys.argv[4]):
        obtenerArchivo = open(sys.argv[4], "rb")
        paqueteArchivo = obtenerArchivo.read(1024)
        while paqueteArchivo:
            
            auxSocket.send(paqueteArchivo)
            paqueteArchivo = obtenerArchivo.read(1024)

        obtenerArchivo.close()

        print("Archivo subido")
    else:
        print("Archivo no existe")


"""Devuelve los archivos que están en el servidor"""
def iniciarEnlistado():
    print("Detección comando listado")
    file_info = auxSocket.recv(1024)
    respuestaServidor = file_info.decode('utf8')
    print(respuestaServidor)
    print("Lista archivos obtenida")

###########################################################################
######################      EJECUCIONES     ###############################
###########################################################################
# Verifica los argumentos
verificarArgumentos()

# Verifica la ip dada
try:
    socket.gethostbyname(sys.argv[1])
except socket.error:
    print("IP inválida")
    sys.exit()
host = sys.argv[1]

# Verifica el puerto
try:
    port = int(sys.argv[2])
except ValueError:
    print("Puerto Inválido")
    sys.exit()
except IndexError:
    print("Puerto Inválido")
    sys.exit()
verificarPuerto()

#Inicializar socket 
try:
    auxSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Socket Cliente inicializado")
    auxSocket.setblocking(0)
    auxSocket.settimeout(15)
except socket.error:
    print("Fallo al crear socket cliente")
    sys.exit()

# Verificar comando
try:
    comando = sys.argv[3]
    if comando != "-d" and comando != "-u" and comando != "-l":
        print("Comando Inválido")
        sys.exit()
    else:
         print("Comando Aceptado: "+ comando )
except ValueError:
    print("Comando Inválido")
    sys.exit()
except IndexError:
    print("Comando Inválido")
    sys.exit()

# Conexión con el servidor
if comando == "-d" or comando == "-u": ##se manda dos parámetros
    command = comando + " "+ sys.argv[4]
else:
    command = comando

CommClient = bytes(command, "utf-8")
print(CommClient)
try:
    auxSocket.connect((host, port))
    auxSocket.send(CommClient)
except ConnectionResetError:
    print( "Incongruencia con los puertos")
    sys.exit()

# División de tareas a partir de comando
if comando == "-d":
    iniciarDescarga()
elif comando =="-u":
    if os.path.isfile(sys.argv[4]):
        iniciarSubida()
    else:
        print("Archivo no existe")
elif comando =="-l":
    iniciarEnlistado()
else:
    try:
        contador, contadorDir = auxSocket.recv(4096)
    except ConnectionResetError:
        print("Los puertos no coinciden")
        sys.exit()
    except:
        print("Tiempo de espera agotado")
        sys.exit()
    text = contador.decode('utf8')
    print(text)

print("¡Aplicación finalizada correctamente!") 
quit()