import socket
import time
import os
import sys

ROOT = '/filesUDP'

"""Utilizada para enlistar todos los archivos que hay en el servidor"""
def listaArchivos():
    print("Solicitud de obtener lista de archivos")
    mensaje = "Comando lista de archivos iniciada"
    mensajeCodificado = mensaje.encode('utf-8')

    auxSocket.sendto(mensajeCodificado, direccionCliente)
    F = os.listdir(ROOT)

    listaArchivos = []
    for file in F:
        listaArchivos.append(file)
    listaStrings = str(listaArchivos)
    listaCodificada = listaStrings.encode('utf-8')
    auxSocket.sendto(listaCodificada, direccionCliente)
    print("Lista enviada al cliente")


"""Funcion utilizada para descargar un archivo para el cliente"""
def descargaArchivo(archivo):
    print("Solicitud de descargar un archivo")
    mensaje = "Comando descarga de archivo iniciada"
    mensajeCodificado = mensaje.encode('utf-8')
    auxSocket.sendto(mensajeCodificado, direccionCliente)

    if os.path.isfile(ROOT+"/"+archivo):
        mensaje = "El archivo existe!"
        mensajeCodificado = mensaje.encode('utf-8')
        auxSocket.sendto(mensajeCodificado, direccionCliente)
        print("El archivo existe")

        # Descomposicion del archivo
        numPaquete = 0
        tamannoPaquete = os.stat(ROOT+"/"+archivo)
        tamannoPaqueteObtenido = tamannoPaquete.st_size  
        print("Tamanno de paquete en bytes:" + str(tamannoPaqueteObtenido))
        totalPaquetes = int(tamannoPaqueteObtenido / 4096) #tam buffer
        totalPaquetes = totalPaquetes + 1

        encapsulamientoPaquete = str(totalPaquetes)
        encapsulamientoPaqueteCodificado = encapsulamientoPaquete.encode('utf8')
        auxSocket.sendto(encapsulamientoPaqueteCodificado, direccionCliente)

        cicloPaquete = int(totalPaquetes)
        obtenerArchivo = open(ROOT+"/"+archivo, "rb")
        while cicloPaquete != 0:
            paqueteArchivo = obtenerArchivo.read(4096)
            auxSocket.sendto(paqueteArchivo, direccionCliente)
            numPaquete += 1
            cicloPaquete -= 1
        obtenerArchivo.close()
        print("Archivo enviado al cliente")

    else:
        mensaje = "El archivo no fue encontrado"
        mensajeCodificado = mensaje.encode('utf-8')
        auxSocket.sendto(mensajeCodificado, direccionCliente)
        print("Imposible enviar archivo al cliente")


"""Usado para subir un archivo"""
def subirArchivo():
    print("Solicitud de subir un archivo")
    mensaje = "Comando subida de archivo iniciada"
    mensajeCodificado = mensaje.encode('utf-8')
    auxSocket.sendto(mensajeCodificado, direccionCliente)

    almacenArchivo = open(ROOT+"/"+listaArgumento[1], "wb")
    try:
        contador, contadorDireccion = auxSocket.recvfrom(4096)  # number of packet
    except ConnectionResetError:
        print("Incongruencia de puertos")
        sys.exit()
    except:
        print("Tiempo agotado para responder")
        sys.exit()

    indiceAlmacen = contador.decode('utf8')
    indiceAlmacen = int(indiceAlmacen)

    while indiceAlmacen != 0:
        infoServidor, dirServidor = auxSocket.recvfrom(4096)
        infoSubida = almacenArchivo.write(infoServidor)
        indiceAlmacen = indiceAlmacen - 1
       
    almacenArchivo.close()
    print("Nuevo archivo subido")


"""Cuando un parametro no existe se corre dicha funcion"""
def incongruenciaParametros():
    mensaje = "La correcta forma de ejecutar es: {IP PUERTO CLAVE(-d, -auxSocket, -l) ARCHIVO}"
    mensajeCodificado = mensaje.encode('utf-8')
    auxSocket.sendto(mensajeCodificado, direccionCliente)
    print("Incongruencia detectada y enviada")


"""Definicion de parametros para el servidor"""
host = ""
port = 5002
try:
    auxSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("Socket del servidor inicializado")
    auxSocket.bind((host, port))
    print("Conexion satisfactoria. Esperando respuesta de cliente")
except socket.error:
    print("Fallo al crear el Socket")
    sys.exit()


"""Funcion que espera envio de datos por parte del cliente"""
while True:
    try:
        data, direccionCliente = auxSocket.recvfrom(4096)
    except ConnectionResetError:
        print("Numero de puerto inconsistente")
        sys.exit()
    texto = data.decode('utf8')
    listaArgumento = texto.split()
    print("")
    print("<-- Nuevo argumento detectado -->")
    print(listaArgumento)
    if listaArgumento[0] == "-d":
        descargaArchivo(listaArgumento[1])
    elif listaArgumento[0] == "-u":
        subirArchivo()
    elif listaArgumento[0] == "-l":
        listaArchivos()
    else:
        incongruenciaParametros()